package com.example.aplicaciondeprueba.Model.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aplicaciondeprueba.Model.Pollo.Pet;
import com.example.aplicaciondeprueba.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterRecyclerView extends RecyclerView.Adapter {
    private List<Pet> listaDePets;

    public AdapterRecyclerView() {
        this.listaDePets = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View celdaInflada = layoutInflater.inflate(R.layout.cell_recycler_view_pets,parent,false);
        ViewHolderPets viewHolderPets = new ViewHolderPets(celdaInflada);
        return viewHolderPets;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Pet pet = listaDePets.get(position);
        ViewHolderPets viewHolderPets = (ViewHolderPets) holder;
        viewHolderPets.cargarPet(pet);
    }


    @Override
    public int getItemCount() {
        return listaDePets.size();
    }



    public class ViewHolderPets extends RecyclerView.ViewHolder{
        private TextView textViewNamePets;


        public ViewHolderPets(@NonNull View itemView) {
            super(itemView);
            textViewNamePets = itemView.findViewById(R.id.textViewName);

        }

        public void cargarPet(Pet pet){
            textViewNamePets.setText(pet.getName());
        }

    }

    public void cargoListaDePets(List<Pet> listaDePetsParaCargar){
        listaDePets.clear();
        listaDePets.addAll(listaDePetsParaCargar);
        notifyDataSetChanged();
    }


}
