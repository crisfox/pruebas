package com.example.aplicaciondeprueba.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.example.aplicaciondeprueba.Model.Adapters.AdapterRecyclerView;
import com.example.aplicaciondeprueba.Model.Pollo.Pet;
import com.example.aplicaciondeprueba.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerViewPet;
    private AdapterRecyclerView adapterRecyclerView;
    private List<Pet> listDePets; //TODO OJO VA A DAR NULL!!!


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Busco el RecyclerView en el xml
        findById();
        //Creo el RecyclerView y le seteo el Layout
        createRecyclerView();

        listDePets = new ArrayList<>();
        for (int i = 0; i < 50 ; i++) {
            Pet pet = new Pet("id"+i,"Pet"+i,"1"+i);
            listDePets.add(pet);
        }

        //Cargo Lista en el Adapter para que se muestre en el ViewHolder
        adapterRecyclerView.cargoListaDePets(listDePets);


    }

    private void findById() {
        recyclerViewPet = recyclerViewPet.findViewById(R.id.recyclerViewList);
    }

    private void createRecyclerView() {
        adapterRecyclerView = new AdapterRecyclerView();
        recyclerViewPet.setAdapter(adapterRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
        recyclerViewPet.setLayoutManager(linearLayoutManager);
        recyclerViewPet.setHasFixedSize(true);
    }
}
